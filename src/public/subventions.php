<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <?php include './includes/css.php'; ?>
        <link rel="icon" type="image/png" href="favicon.png" />

        <title>Gîte du millepertuis, subventions / travaux soutenus</title>
        <meta name="description" content="Retrouvez toutes nos subventions - Aides et fonds Européens et régionales">
        <meta name="keywords" content="Gîte, Auvergne, Sancy, Chastreix, Ferme, Campagne">

        <link rel="canonical" href="<?= ($_SERVER['HTTPS'] ? 'https' : 'http') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>" />
    </head>

    <body class="body">
        <?php include './includes/header.php'; ?>

        <main>
            <div class="container pt-5 px-4">
                <div class="row">
                    <div class="col-12 mb-3 bg-white p-3">
                        <section>
                            <h1>Aides et fonds Européens</h1>
                            <div class="financing">
                                <div class="row">
                                    <div class="col mb-3 mb-md-4 d-flex justify-content-center align-items-center">
                                        <a href="http://ec.europa.eu/agriculture/rural-development-2014-2020/index_fr.htm"><img class="financing__img" src="build/images/logo/fonds-europeen-agricole.ef38e9a7.jpg" loading="lazy" title="Logo du fonds Européen Agricole pour le développement rural" alt="Logo du fonds Européen Agricole pour le développement rural"/></a>
                                    </div>
                                    <div class="col mb-3 mb-md-4 d-flex justify-content-center align-items-center">
                                        <a href="http://ec.europa.eu/agriculture/rural-development-2014-2020/index_fr.htm"><img class="financing__img" src="build/images/logo/europe-feader.231da516.jpg" loading="lazy" title="Logo de l'engagement Européen dans la région AURA" alt="L'Europe s'engage en région Auvergne-Rhône-Alpes avec le FEADER"/></a>
                                    </div>
                                    <div class="col mb-3 mb-md-4 d-flex justify-content-center align-items-center">
                                        <a href="https://www.auvergnerhonealpes.fr/"><img class="financing__img" src="build/images/logo/region-aura.7dd7a9e4.png" loading="lazy" title="Logo de la région Auvergne-Rhône-Alpes" alt="Logo de la région Auvergne-Rhône-Alpes"/></a>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section>
                            <h2>Les travaux soutenus par l'UE</h2>
                            <p>L'Europe et la région Auvergne-Rhône-Alpes se sont engagés à financer une partie de la rénovation d'une ancienne maison pour en faire un gîte.</p>
                            <p>Ce financement a permis de changer l'intégralité des portes et fenêtres qui étaient en simple vitrage pour les remplacer par du double vitrage ainsi que de faire de l'isolation. L'ensemble permet de réduire la consommation d'électricité et une meilleure isolation thermique et ainsi de réduire les émissions de CO&#8322;.</p>
                            <p>Cette aide a aussi permis de faire de l'aménagement intérieur comme les peintures, la plomberie, l'électricité, etc</p>
                            <hr/>
                            <p>Le site de la comission relatif au FEADER : <a href="http://ec.europa.eu/agriculture/rural-development-2014-2020/index_fr.htm">Rural development : protecting the future of rural communities</a>.</p>
                        </section>
                    </div>
                </div>
            </div>
        </main>

        <?php include './includes/footer.php'; ?>

        <?php include './includes/js.php'; ?>
    </body>
</html>
