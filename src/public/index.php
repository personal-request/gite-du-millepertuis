<?php
$indexActive = ' active';
?>

<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <?php include './includes/css.php'; ?>
        <link rel="icon" type="image/png" href="favicon.png" />

        <title>Gîte du millepertuis, accueil à la ferme à proximité du Sancy</title>
        <meta name="description" content="Aline et Bruno vous accueillent au cœur des volcans d'Auvergne, à Chastreix, dans un environnement paisible, au pied des montagnes du Sancy.">
        <meta name="keywords" content="Gîte, Auvergne, Sancy, Chastreix, Ferme, Campagne">

        <link rel="canonical" href="<?= ($_SERVER['HTTPS'] ? 'https' : 'http') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>" />
    </head>

    <body class="body">
        <?php include './includes/header.php'; ?>

        <main>
            <div class="container pt-5 px-4">
                <div class="row">
                    <div class="col-12 mb-3 bg-white p-3">
                        <section>
                            <h1>Bienvenue au Gîte du Millepertuis</h1>
                            <p>Aline et Bruno vous accueillent en <strong>Auvergne</strong>, à Chastreix. Leur <strong>gîte</strong> est aménagé dans une petite maison rénovée du XIXᵉ siècle. D'une <strong>capacité de 10 couchages</strong>, il se situe au bout de la route, dans un environnement paisible, au pied des <strong>montagnes du Sancy</strong>. Il se situe à 50m de leur <strong>ferme</strong>, en <strong>Agriculture Biologique</strong>, où ils produisent du <strong>Saint-Nectaire</strong> grâce à une vingtaine de vaches.</p>
                        </section>
                    </div>
                    <div class="col-md-6 mb-3 bg-white p-3">
                        <section>
                            <h2>Capacité</h2>
                            <p>10 personnes | Idéalement conçu pour 6 à 8 personnes</p>

                        </section>
                    </div>
                    <div class="col-md-6 mb-3 bg-white p-3">
                        <section>
                            <h2>Accueil - Départ</h2>
                            <table class="table table-hover">
                                <thead class="table-secondary">
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Samedi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">Accueil</th>
                                        <td>À partir de 14h</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Départ</th>
                                        <td>Jusqu'à 11h</td>
                                    </tr>
                                </tbody>
                            </table>
                        </section>
                    </div>
                    <div class="col-12 mb-3 bg-white p-3">
                        <section>
                            <h2>Composition</h2>
                            <p>La maison est composée de deux niveaux avec un <strong>jardin</strong>. À l'intérieur vous trouverez :</p>
                            <ul>
                                <li>Une grande <strong>pièce de vie</strong> avec : cuisine, salle à manger et un <strong>poêle à bois</strong> pour le chauffage</li>
                                <li>Une chambre/salon équipé d'un canapé/lit (BZ) en 160, d'un <strong>TV</strong>, d'un bureau, d'une armoire et d'un chauffage électrique</li>
                                <li>Une chambre avec un lit double étage en 90*, un lit en 140 ainsi qu'un bureau, une armoire et un chauffage électrique</li>
                                <li>Une troisième chambre, avec un lit double étage en 90*, un lit en 160 et aussi un bureau, une armoire et un chauffage électrique</li>
                                <li>
                                    Deux salles d'eau :
                                    <ul>
                                        <li>La première en rez-de-chaussé avec une baignoire de 170, un WC, un lavabo et un lave linge</li>
                                        <li>La deuxième à l'étage, équipée d'une douche et d'un lavabo</li>
                                        <li>Un deuxième WC, séparé, se situe à l'étage</li>
                                    </ul>
                                </li>
                            </ul>
                            <p class="text-muted">* les lits du dessus ne conviennent pas aux enfants de moins de 6 ans</p>
                        </section>
                    </div>
                    <div class="col-12 mb-3 bg-white p-3">
                        <section>
                            <h2>Équipement</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <p class="card-title">Intérieur</p>
                                            <ul>
                                                <li><strong>TV</strong></li>
                                                <li>Lave vaisselle</li>
                                                <li>Lave linge</li>
                                                <li>Cuisinière (3 gaz, 1 électrique) et Four</li>
                                                <li>Réfrigérateur/congélateur</li>
                                                <li>
                                                    Électro ménager divers :
                                                    <ul>
                                                        <li><strong>Appareil à fondue</strong></li>
                                                        <li><strong>Appareil à raclette</strong></li>
                                                        <li>Bouilloire</li>
                                                        <li>Cafetière</li>
                                                        <li>Cuit vapeur</li>
                                                        <li><strong>Crépière</strong></li>
                                                        <li>Gaufrier / Croque monsieur</li>
                                                        <li>Grille pain</li>
                                                        <li>Micro-onde</li>
                                                        <li>Mixeur plogeant</li>
                                                        <li>Robot de cuisine</li>
                                                    </ul>
                                                </li>
                                                <li>Aspirateur</li>
                                                <li><strong>Matériel de bébé</strong> à la demande</li>
                                                <li><strong>Jeux de societé</strong> à la demande</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <p class="card-title">Éxtérieur</p>
                                            <ul>
                                                <li><strong>Jardin</strong></li>
                                                <li><strong>Terrasse</strong></li>
                                                <li>Table de jardin</li>
                                                <li><strong>Barbecue</strong></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-body">
                                            <p class="card-title">Services</p>
                                            <ul>
                                                <li>Wifi - Internet</li>
                                                <li>Chaînes françaises et régionales en satellite</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </section>
                    </div>
                    <div class="col-12 mb-3 bg-white p-3">
                        <section>
                            <h2>Les atouts</h2>
                            <ul class="list-group mb-4">
                                <li class="list-group-item">L'hébergement se situe dans un petit hameau au sein du <strong>Parc Naturel Régional des Volcans d'Auvergne</strong>, à moins d'un km de la <strong>Réserve Naturelle Nationale de Chastreix-Sancy</strong>.</li>
                                <li class="list-group-item">Possibilité de départ immédiat en <strong>balades et randonnées</strong>.</li>
                                <li class="list-group-item">Ruisseau à proximité.</li>
                                <li class="list-group-item">Possibilité (si la neige le permet) de <strong>faire de la luge</strong> dans les prés à côté de la maison.</li>
                                <li class="list-group-item">Possibilité de <strong>voir la traite et la fabrication du St-Nectaire</strong>.</li>
                            </ul>
                        </section>
                    </div>
                </div>
            </div>
        </main>

        <?php include './includes/footer.php'; ?>

        <?php include './includes/js.php'; ?>
    </body>
</html>
