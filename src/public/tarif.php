<?php
$priceActive = ' active';
?>

<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <?php include './includes/css.php'; ?>
        <link rel="icon" type="image/png" href="favicon.png" />

        <title>Gîte du millepertuis, tous nos tarifs</title>
        <meta name="description" content="Nos tarifs se situent entre 700 et 800&nbsp;€ la semaine en fonction de la période.">
        <meta name="keywords" content="Gîte, Auvergne, Sancy, Chastreix, Ferme, Campagne, Tarifs">

        <link rel="canonical" href="<?= ($_SERVER['HTTPS'] ? 'https' : 'http') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>" />
    </head>

    <body class="body">
        <?php include './includes/header.php'; ?>

        <main>
            <div class="container pt-5 px-4">
                <div class="row">
                    <div class="col-12 mb-3 bg-white p-3">
                        <section>
                            <h1>Tarifs</h1>
                            <p>Taxe de séjour* : 1,10&nbsp;€/jour/personne de plus de 18&nbsp;ans.</p>
                            <p class="text-muted">* À prévoir en plus des tarifs ci-dessous. La taxe de séjour est reversée à l'office de tourisme.</p>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <h3>Semaine complète</h3>
                                            <table class="table table-hover">
                                                <thead class="table-secondary">
                                                <tr>
                                                    <th scope="col">Période</th>
                                                    <th scope="col">Tarif</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <th scope="row">Du 1er octobre au 31 mai</th>
                                                    <td>800&nbsp;€</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Du 1er juin au 30 septembre</th>
                                                    <td>700&nbsp;€</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <h3>Week-End (2 jours)</h3>
                                            <table class="table table-hover">
                                                <thead class="table-secondary">
                                                <tr>
                                                    <th scope="col">Période</th>
                                                    <th scope="col">Tarif</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <th scope="row">Mai, Octobre, Novembre, Décembre</th>
                                                    <td>300&nbsp;€</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Jour supplémentaire</th>
                                                    <td>130&nbsp;€</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="card">
                                        <div class="card-body">
                                            <h3>Services supplémentaires</h3>
                                            <table class="table table-hover">
                                                <thead class="table-secondary">
                                                <tr>
                                                    <th scope="col">Service</th>
                                                    <th scope="col">Tarif</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <th scope="row">Draps de lit 140 ou 160<br/>(drap du dessous + housse de couette)</th>
                                                    <td>12€/lit</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Draps de lit 90<br/>(drap du dessous + housse de couette)</th>
                                                    <td>8€/lit</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Linge de toilette</th>
                                                    <td>6€/personne</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Forfait ménage</th>
                                                    <td>80€</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </di>
                                </div>
                            </div>
                        </section>
                    </div>

                    <div class="col-12 mb-3 bg-white p-3">
                        <section>
                            <h2>Paiement</h2>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="card">
                                        <div class="card-header">
                                            À la semaine
                                        </div>
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">400&nbsp;€ à la réservation (acompte)</li>
                                            <li class="list-group-item">Le solde à l'arrivée</li>
                                            <li class="list-group-item">300&nbsp;€ de caution à l'arrivée</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="card">
                                        <div class="card-header">
                                            Week-End
                                        </div>
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">200&nbsp;€ à la réservation (acompte)</li>
                                            <li class="list-group-item">Le solde à l'arrivée</li>
                                            <li class="list-group-item">300&nbsp;€ de caution à l'arrivée</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    <div class="col-12 mb-3 bg-white p-3">
                        <section>
                            <h2>Moyens de paiement</h2>
                            <div class="row">
                                <div class="col-lg-6">
                                    <ul class="list-group mb-4">
                                        <li class="list-group-item">Par chèque.</li>
                                        <li class="list-group-item">Par virement*.</li>
                                    </ul>
                                </div>
                            </div>
                            <p class="text-muted ml-4">* IBAN/RIB à demander</p>
                        </section>
                    </div>
                </div>
            </div>
        </main>

        <?php include './includes/footer.php'; ?>

        <?php include './includes/js.php'; ?>
    </body>
</html>
