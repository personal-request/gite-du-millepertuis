<?php
$photoActive = ' active';
?>

<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <?php include './includes/css.php'; ?>
        <link rel="icon" type="image/png" href="favicon.png" />

        <title>Gîte du millepertuis, photos intérieurs et des alentours</title>
        <meta name="description" content="Retrouvez des photos du Gîte du Millepertuis, se situant à 1150m d'altitude, au cœur du Sancy.">
        <meta name="keywords" content="Gîte, Auvergne, Sancy, Chastreix, Ferme, Campagne, Photos">

        <link rel="canonical" href="<?= ($_SERVER['HTTPS'] ? 'https' : 'http') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>" />
    </head>

    <body class="body">
        <?php include './includes/header.php'; ?>

        <main>
            <div class="container pt-5 px-4">
                <div class="row">
                    <div class="col-12 mb-5 bg-white p-3">
                        <h1>Quelques photos</h1>
                    </div>
                    <div class="col-12 mb-5 bg-white p-3">
                        <section>
                            <h2>Gîte - Extérieur</h2>
                            <div id="carouselGiteOutside" class="carousel slide">
                                <div class="carousel-indicators">
                                    <button type="button" data-bs-target="#carouselGiteOutside" data-bs-slide-to="0" class="active"></button>
                                    <button type="button" data-bs-target="#carouselGiteOutside" data-bs-slide-to="1"></button>
                                    <button type="button" data-bs-target="#carouselGiteOutside" data-bs-slide-to="2"></button>
                                </div>
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img src="media/images/outside/gite-outside-1.jpg" class="d-block" loading="lazy" title="Gîte extérieur" alt="Le gîte vu de face, en été. On peut y voir des volets rouges à chaque fenêtre et une partie très fleurie.">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="media/images/outside/gite-outside-2.jpg" class="d-block" loading="lazy" title="Gîte extérieur" alt="Le gîte vu de côté, en été. On peut y voir des volets rouges aux différentes fenêtres. Devant, se trouve une petite terrasse. Derrière, on peut voir de grands arbres verts.">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="media/images/outside/gite-outside-snow-1.jpg" class="d-block" loading="lazy" title="Gîte extérieur en hiver, avec de la neige" alt="Le gîte vu de côté, en hiver. Les arbres n'ont plus de feuilles et une épaisse couche de neige recouvre le sol.">
                                    </div>
                                </div>
                                <button class="carousel-control-prev" type="button" data-bs-target="#carouselGiteOutside" data-bs-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Image précédente</span>
                                </button>
                                <button class="carousel-control-next" type="button" data-bs-target="#carouselGiteOutside" data-bs-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Image suivante</span>
                                </button>
                            </div>
                        </section>
                    </div>

                    <div class="col-12 mb-5 bg-white p-3">
                        <section>
                            <h2>Gîte - Intérieur</h2>
                            <div id="carouselGiteInside" class="carousel slide">
                                <div class="carousel-indicators">
                                    <button type="button" data-bs-target="#carouselGiteInside" data-bs-slide-to="0" class="active"></button>
                                    <button type="button" data-bs-target="#carouselGiteInside" data-bs-slide-to="1"></button>
                                    <button type="button" data-bs-target="#carouselGiteInside" data-bs-slide-to="2"></button>
                                    <button type="button" data-bs-target="#carouselGiteInside" data-bs-slide-to="3"></button>
                                    <button type="button" data-bs-target="#carouselGiteInside" data-bs-slide-to="4"></button>
                                    <button type="button" data-bs-target="#carouselGiteInside" data-bs-slide-to="5"></button>
                                    <button type="button" data-bs-target="#carouselGiteInside" data-bs-slide-to="6"></button>
                                    <button type="button" data-bs-target="#carouselGiteInside" data-bs-slide-to="7"></button>
                                    <button type="button" data-bs-target="#carouselGiteInside" data-bs-slide-to="8"></button>
                                    <button type="button" data-bs-target="#carouselGiteInside" data-bs-slide-to="9"></button>
                                    <button type="button" data-bs-target="#carouselGiteInside" data-bs-slide-to="10"></button>
                                    <button type="button" data-bs-target="#carouselGiteInside" data-bs-slide-to="11"></button>
                                    <button type="button" data-bs-target="#carouselGiteInside" data-bs-slide-to="12"></button>
                                </div>
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img src="media/images/inside/dining-room-1.jpg" class="d-block" loading="lazy" title="Salle-à-manger et cuisine" alt="La salle-à-manger se constitue d'une grande table avec deux grands bancs qui la longe. Derrière, la cuisine, toute équipée.">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="media/images/inside/main-room-1.jpg" class="d-block" loading="lazy" title="Salle principale - détente" alt="La salle principale contient deux fauteuils, devant un poêle à bois.">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="media/images/inside/dining-room-2.jpg" class="d-block" loading="lazy" title="Pièce de vie" alt="En plus des deux fauteuils et de la table à manger, on peut voir le poêle allumé, avec des morceaux de bois à l'arrière, pour alimenter le feu.">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="media/images/inside/main-room-2.jpg" class="d-block" loading="lazy" title="Salle principale" alt="Un poêle à bois se situe à la place du foyer, dans une cheminée comme on pouvait retrouver dans les anciennes maisons.">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="media/images/inside/bedroom-n1-1.jpg" class="d-block" loading="lazy" title="Chambre 1 (photo 1)" alt="Dans cette chambre, on peut voir un canapé bleu. Il s'agit d'un canapé de type BZ permettant de faire un lit, pour deux personnes.">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="media/images/inside/bedroom-n1-2.jpg" class="d-block" loading="lazy" title="Chambre 1 (photo 2)" alt="En plus du canapé BZ, ici déplié, on retrouve une télé, en hauteur, sur une ancienne cheminée. Une armoire est aussi disponible">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="media/images/inside/bedroom-n2-1.jpg" class="d-block" loading="lazy" title="Chambre 2 (photo 1)" alt="La deuxième chambre contient un lit 2 places, ainsi qu'un double lit superposé. Une armoire est aussi présente.">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="media/images/inside/bedroom-n2-2.jpg" class="d-block" loading="lazy" title="Chambre 2 (photo 2)" alt="En plus des deux lits, on peut voir un petit bureau.">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="media/images/inside/bedroom-n3-1.jpg" class="d-block" loading="lazy" title="Chambre 3 (photo 1)" alt="La troisième chambre contient un lit 2 places, ainsi qu'un double lit superposé. Il y a aussi une armoire.">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="media/images/inside/bedroom-n3-2.jpg" class="d-block" loading="lazy" title="Chambre 3 (photo 2)" alt="La troisième chambre contient un lit 2 places, ainsi qu'un double lit superposé.">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="media/images/inside/bathroom-first-floor-1.jpg" class="d-block" loading="lazy" title="Salle d'eau - Rez-de-chaussée" alt="La salle d'eau du rez-de-chaussée contient des toilettes, une vasque et une baignoire. Le carrelage est vert.">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="media/images/inside/bathroom-floor1-1.jpg" class="d-block" loading="lazy" title="Salle d'eau - Étage" alt="La salle d'eau de l'étage, faite de carrelage bleu, contient une douche et une vasque.">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="media/images/inside/toilet-floor1-1.jpg" class="d-block" loading="lazy" title="Toilettes - Étage" alt="Les toilettes de l'étage sont séparés de la salle d'eau.">
                                    </div>
                                </div>
                                <button class="carousel-control-prev" type="button" data-bs-target="#carouselGiteInside" data-bs-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Image précédente</span>
                                </button>
                                <button class="carousel-control-next" type="button" data-bs-target="#carouselGiteInside" data-bs-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Image suivante</span>
                                </button>
                            </div>
                        </section>
                    </div>

                    <div class="col-12 mb-5 bg-white p-3">
                        <section>
                            <h2>Autres photos</h2>
                            <div id="carouselOther" class="carousel slide" data-ride="carousel" data-interval="false">
                                <div class="carousel-indicators">
                                    <button type="button" data-bs-target="#carouselOther" data-bs-slide-to="0" class="active"></button>
                                    <button type="button" data-bs-target="#carouselOther" data-bs-slide-to="1"></button>
                                    <button type="button" data-bs-target="#carouselOther" data-bs-slide-to="2"></button>
                                    <button type="button" data-bs-target="#carouselOther" data-bs-slide-to="3"></button>
                                    <button type="button" data-bs-target="#carouselOther" data-bs-slide-to="4"></button>
                                    <button type="button" data-bs-target="#carouselOther" data-bs-slide-to="5"></button>
                                </div>
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img src="media/images/other/landscape-from-gite-1.jpg" class="d-block" loading="lazy" title="Paysage vu du gîte" alt="Photo du Mont Redon, faisant parti du Massif du Sancy, au printemps. Visible depuis le Gîte.">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="media/images/other/landscape-from-end-valley-1.jpg" class="d-block" loading="lazy" title="Paysage vu du fond de la vallée où se situe le gite" alt="Paysage en été, de la vallée dans laquelle se trouve le gîte.">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="media/images/other/cows-close-by-1.jpg" class="d-block" loading="lazy" title="Vaches de la ferme" alt="Des vaches se prélassent dans un près à côté du gîte et de la ferme.">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="media/images/other/cows-in-montains.jpg" class="d-block" loading="lazy" title="Vaches de la ferme à la montage" alt="Des vaches au milieu des montagnes.">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="media/images/other/collecting-small-bales-of-hay-1.jpg" class="d-block" loading="lazy" title="Ramassage des petites bottes de foins" alt="Plusieurs tas de petites bottes de foins attendent d'être ramassées, comme on peut le voir dans la remorque en arrière-plan.">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="media/images/other/ouchka-farm-dog-1.jpg" class="d-block" loading="lazy" title="Ouchka, la chienne de la ferme" alt="Une chienne, noire et blanche, est couchée sur un ballot de foin.">
                                    </div>
                                </div>
                                <button class="carousel-control-prev" type="button" data-bs-target="#carouselOther" data-bs-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Image précédente</span>
                                </button>
                                <button class="carousel-control-next" type="button" data-bs-target="#carouselOther" data-bs-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Image suivante</span>
                                </button>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </main>

        <?php include './includes/footer.php'; ?>

        <?php include './includes/js.php'; ?>
    </body>
</html>
